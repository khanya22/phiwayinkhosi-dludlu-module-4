import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);
  static const routeName = '/sigup';
  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Signup",
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text("Sign Up"),
            titleTextStyle: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
            leading: const Icon(
              Icons.person_rounded,
              color: Colors.black,
              size: 25,
            ),
            backgroundColor: Colors.white,
            elevation: 0,
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                      decoration: InputDecoration(
                          label: Text("Name"), icon: Icon(Icons.person))),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                      decoration: InputDecoration(
                          label: Text("Email"), icon: Icon(Icons.email))),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                    decoration: InputDecoration(
                        label: Text("Password"),
                        icon: Icon(
                          Icons.password,
                        )),
                    obscureText: true,
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(15),
                    child: TextField(
                      decoration: InputDecoration(
                        label: Text("Confrim Password"),
                        icon: Icon(Icons.password),
                      ),
                      obscureText: true,
                    )),
              ],
              mainAxisSize: MainAxisSize.min,
            ),
          ),
          persistentFooterButtons: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/login');
                },
                child: const Text(
                  "<Back",
                  style: TextStyle(decoration: TextDecoration.underline),
                )),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/dashboard');
                  },
                  child: const Text(
                    "Confirm",
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  style: ButtonStyle(alignment: Alignment.centerLeft)),
            )
          ],
        ));
  }
}

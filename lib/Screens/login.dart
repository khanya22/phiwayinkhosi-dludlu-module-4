import 'package:flutter/material.dart';
import 'package:myapp/Screens/dashboard.dart';
import 'package:myapp/Screens/signup.dart';

void main() => runApp(Login());

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);
  static const routeName = '/login';

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String email = '';
  String password = '';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "African Black Child",
        home: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(15),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Image.asset(
                        "assets/logo.jpg",
                        height: 100,
                        width: 100,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: TextField(
                        onChanged: (value) => email = value,
                        decoration: const InputDecoration(
                            labelText: "Enter email",
                            alignLabelWithHint: true,
                            border: OutlineInputBorder()),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: TextField(
                        onChanged: (value) => password = value,
                        decoration: const InputDecoration(
                            labelText: "Enter Password",
                            border: OutlineInputBorder()),
                        obscureText: true,
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          //Navigate to dashboard.dart
                          Navigator.of(context).pushNamed(Dashboard.routeName);
                        },
                        style: ElevatedButton.styleFrom(primary: Colors.black),
                        child: const Text("Login",
                            style: TextStyle(color: Colors.white))),
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
              ),
            ),
          ),
          extendBody: false,
          backgroundColor: Colors.white,
          persistentFooterButtons: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/signup');
                },
                child: const Text(
                  "Sign Up",
                  style: TextStyle(color: Color.fromARGB(255, 8, 43, 199)),
                )),
          ],
        ));
  }
}
